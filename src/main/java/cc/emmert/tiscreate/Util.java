package cc.emmert.tiscreate;

public class Util {
    public static short clamp(int n) {
        return (short) Math.max(Math.min(n,Short.MAX_VALUE),Short.MIN_VALUE);
    }

    public static short clamp(float n) {
        return (short) Math.max(Math.min(Math.round(n),Short.MAX_VALUE),Short.MIN_VALUE);
    }
}
